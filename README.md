# semihalfino

*Copyright (c) 2023 Semihalf.*

### LICENSE(s)

Hardware project located in folder ***pcb_project*** is distributed under CERN Open Hardware Licence Version 2 - Strongly Reciprocal (CERN-OHL-S).

Firmware project located in folder ***demo*** is distributed under 2-Clause BSD License (BSD 2-Clause).

Photos, pictures and all other documentation located in folder ***doc*** is distributed under Creative Commons Attribution-ShareAlike 4.0 International License (CC-BY-SA-4.0).

![semihalfino photo](doc/semihalfino.jpg)

### Authors

Mirosław Folejewski

Krystyna Oniszczuk-Dylik

Igor Jankiewicz

Michał Barnaś

Radosław Górniak

Jacek Majkowski

### How to start?

A step-by-step guide is provided below.

#### When you already have the **semihalfino** board
##### On Windows
1. Install [Raspberry Pi Pico C IDE](https://www.raspberrypi.com/news/raspberry-pi-pico-windows-installer/)
2. Run *Command Prompt* from menu Start
3. Place the folder with the demo source code folder in the same directory as the pico-sdk folder. <br>
        *> cd c:/* <br>
        *> git clone https://gitlab.com/semihalf-open-hardware/semihalfino.git* <br>
        *> cd semihalfino* <br>
        *> git clone https://github.com/raspberrypi/pico-sdk.git --branch master* <br>
        *> cd pico-sdk* <br>
        *> git submodule update --init* <br>
4. Run *Pico – Visual Studio Code* from menu Start
5. In *Pico – Visual Studio Code* from menu File > Open folder - select folder *C:\semihalfino\demo*
6. In *Pico – Visual Studio Code* build the application. <br>
If compilation fails, you may need to adjust the path to pico-sdk in the file CMakeLists.txt.
7. If the compilation passed, the file main.uf2 will appear in the build folder.
8. To download the application to semihalfino, press and hold the BOOT button on the board (do not release it), then press the RESET button, release the RESET button and then release the BOOT button.
9. Now semihalfino should be recognized as mass storage device (a flash drive) with name RPI-RP2, just open it in Windows Explorer.
10. Copy the  main.uf2 file to the flash drive.

##### On Linux
1. Install the Raspberry Pi Pico C SDK (more info is [here](https://datasheets.raspberrypi.com/pico/getting-started-with-pico.pdf)). <br>
        *$ sudo apt update* <br>
        *$ sudo apt install cmake gcc-arm-none-eabi libnewlib-arm-none-eabi build-essential* <br>
2. Place the folder with the demo source code folder in the same directory as the pico-sdk folder. <br>
        *$ cd ~/* <br>
        *$ git clone https://gitlab.com/semihalf-open-hardware/semihalfino.git* <br>
        *$ cd semihalfino* <br>
        *$ git clone https://github.com/raspberrypi/pico-sdk.git --branch master* <br>
        *$ cd pico-sdk* <br>
        *$ git submodule update --init* <br>
3. Compile demo application.<br>
        *$ cd ~/semihalfino/demo* <br>
        *$ mkdir build* <br>
        *$ cd build* <br>
        *$ cmake ..* <br>
        *$ make -j4* <br>
If compilation fails, you may need to adjust the path to pico-sdk in the file CMakeLists.txt.
4. If the compilation passed, the file main.uf2 will appear in the build folder.
5. To download the application to semihalfino, press and hold the BOOT button on the board (do not release it), then press the RESET button, release the RESET button and then release the BOOT button.
6. Now semihalfino should be recognized as mass storage device (a flash drive) - just mount it in Linux. <br>
        *$ sudo blkid -o list | grep RPI-RP2* <br>
        *$ sudo mkdir -p /mnt/pico* <br>
        *$ sudo mount /dev/sda1 /mnt/pico* <br>
You may need to adapt the device name */dev/sda1* to the drive name displayed by the *blkid* command above.<br>
        *$ sudo cp main.uf2 /mnt/pico* <br>

#### If you need to get your own **semihalfino** board
1. In folder ***pcb_project*** is located the PCB project created in KiCad 7.0
2. Use the KiCad project and order your own PCB, components and make the **semihalfino** by yourself or order completely assembled PCB based on the data provided.

-----

**Please contribute** - create a project based on semihalfino and share it!


