"""
Copyright (c) 2023 Semihalf.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

#!/usr/bin/env python3
import sys

class Font():
	def __init__(self):
		self.width = 0
		self.bytes = []

fontData = {}

with open(sys.argv[1]) as f:
	while True:
		line = f.readline()
		if not line:
			break

		character = line[0]
		values = []
		for a in range(0,7):
			value = f.readline().replace("\n", "")
			values.append(value)

		obj = Font()
		obj.width = len(values[0])

		for a in range(0, obj.width):
			byte = 0
			for b in range(0, 7):
				if values[b][a] == '1':
					byte = byte | (1 << b)
			obj.bytes.append(byte)

		fontData[ord(character)] = obj

with open("font_gen.cpp", "w") as out:
	out.write('#include "../Font.hpp"\n\n')
	out.write('FontNode font[] = {')
	for a in range(0, 128):
		if a in fontData:
			out.write("\t{{ .width = {}, .font = {} }},\n".format(fontData[a].width, str(fontData[a].bytes).replace("[", "{").replace("]", "}")))
		else:
			out.write("\t{ .width = 0 },\n")
	out.write('};\n')
